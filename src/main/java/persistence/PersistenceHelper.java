package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PersistenceHelper {
	
	public String username;
	public String password;
	public Connection connection;
	public String resultado;
	
	public PersistenceHelper(String username, String password){
		connection = null;
		resultado = "not";
		try{
			Class.forName("org.postgresql.Driver");
			//resultado = "yah";
		}catch(ClassNotFoundException e){
			e.printStackTrace();
			return;
		}
		
		try{
			this.username = username;
			this.password = password;
			connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/SARC2", this.username, this.password);
			resultado = "yah";
		}catch(SQLException e){
			e.printStackTrace();
			return;
		}
	}
	
	
}
