package util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import demo.models.Package;
import demo.models.Status;
import persistence.PersistenceHelper;

public class DemoUtil {
	private PersistenceHelper persistenceHelper = new PersistenceHelper("postgres", "c27u09m83p");
	List<Package> paquetes = new ArrayList<>();
	
	public DemoUtil(){
		
	}
	
	public int login(String username, String password) throws SQLException{
		String loginQuery = "SELECT id FROM inbox.usuario WHERE nombre='" + username + "' AND password='"+ password + "'";
		Statement statement = persistenceHelper.connection.createStatement();
		ResultSet rs = statement.executeQuery(loginQuery);
		if(rs.next()){
			
			return rs.getInt("id");		
		}
		return -1;
	}
	
	public List<Package> getTareas() throws SQLException{
		String tareasNotDoneQuery = "SELECT * FROM gui_paquete";
		Statement statement = persistenceHelper.connection.createStatement();
		ResultSet rs = statement.executeQuery(tareasNotDoneQuery);
		List<Package> packages = new ArrayList<>();
		String result = "";
		//ArrayList<String> tareasUndone = new ArrayList<>();
		
		while(rs.next()){
			Package paquete = new Package();
			paquete.setId(rs.getInt("id"));
			paquete.setCertificar(rs.getBoolean("certificar"));
			paquete.setStatus(new Status(rs.getString("operative_status"), rs.getString("process_status")));
			paquete.setPayload(buildPayload(rs.getString("payload")));
			packages.add(paquete);
		}
		return packages;
	}
	
	public void agregarTarea(String nombreTarea, int proceso_id, int usuario_id, int status_id) throws SQLException{
		String addTareaQuery = "INSERT INTO inbox.tarea VALUES ('" + nombreTarea + "', " + proceso_id + ", " + usuario_id + ", " + status_id + ")";
		Statement statement = persistenceHelper.connection.createStatement();
		statement.executeUpdate(addTareaQuery);
	}
	
	private boolean verificarTarea(int tarea_id) throws SQLException{
		String verifTareaQuery = "SELECT nombre FROM inbox.tarea WHERE id=" + tarea_id;
		Statement statement = persistenceHelper.connection.createStatement();
		ResultSet rs = statement.executeQuery(verifTareaQuery);
		return rs.next();
	}
	
	/*public void escalarTarea(int tarea_id, int usuario_id){
		String escalarTarea = "UPDATE inbox.tarea SET usuario_id=" + usuario_id + ", status_id=1 WHERE id=" + tarea_id ; 
		if(verificarTarea(tarea_id)){
			
		}
		
	}*/
	
	public Package parsePackage(String json){
		return new Gson().fromJson(json, Package.class);
	}
	
	public Map<String, String> buildPayload(String json){
		return new Gson().fromJson(json, Map.class);
		
	}
	
	public boolean updateJob(String json) throws SQLException{
		Package paquete = parsePackage(json);
		String updateJobQuery = "UPDATE gui_paquete SET id=" + paquete.getId() + "," +
				"operative_status='" + paquete.getStatus().getOperative() + "'," +
				"process_status='" + paquete.getStatus().getProcess() + "'," +
				"payload='" + paquete.toStringPayload() + "'" +
				" WHERE id=" + paquete.getId(); 
		Statement statement = persistenceHelper.connection.createStatement();
		statement.execute(updateJobQuery);
		return true;
	}
	
	public Package updateJob2(String json) throws SQLException{
		Package paquete = parsePackage(json);
		String updateJobQuery = "UPDATE gui_paquete SET id=" + paquete.getId() + "," +
				"operative_status='" + paquete.getStatus().getOperative() + "'," +
				"process_status='" + paquete.getStatus().getProcess() + "'," +
				"payload='" + paquete.toStringPayload() + "'" +
				" WHERE id=" + paquete.getId() + " AND certificar=TRUE";
		Statement statement = persistenceHelper.connection.createStatement();
		statement.execute(updateJobQuery);
		return paquete;
	}
	
	public boolean updateJob(Package paquete) throws SQLException{
		String updateJobQuery = "UPDATE gui_paquete SET id=" + paquete.getId() + "," +
				"operative_status='" + paquete.getStatus().getOperative() + "'," +
				"process_status='" + paquete.getStatus().getProcess() + "'," +
				"payload='" + paquete.toStringPayload() + "'" +
				" WHERE id=" + paquete.getId(); 
		Statement statement = persistenceHelper.connection.createStatement();
		statement.execute(updateJobQuery);
		return true;
	}
	
	public Package getPackage(int id) throws SQLException{
		String getPackageQuery = "SELECT * FROM gui_paquete WHERE id=" + id;
		Statement statement = persistenceHelper.connection.createStatement();
		ResultSet rs = statement.executeQuery(getPackageQuery);
		Package paquete = new Package();
		if(rs.next()){
			paquete.setId(rs.getInt("id"));
			paquete.setCertificar(rs.getBoolean("certificar"));
			paquete.setStatus(new Status(rs.getString("operative_status"), rs.getString("process_status")));
			paquete.setPayload(buildPayload(rs.getString("payload")));
			return paquete;
		}
		return null;
	}
}
