package demo.models;

import java.util.Map;

import com.google.gson.Gson;

public class Package {
	private int id;
	private Status status;
	private boolean certificar;
	private Map<String, String> payload;
	
	public Package() {
		super();
	}
	
	public Package(int id, Status status, boolean certificar, Map<String, String> payload) {
		this.id = id;
		this.status = status;
		this.payload = payload;
		this.certificar = certificar;
	}



	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Map<String, String> getPayload() {
		return payload;
	}

	public void setPayload(Map<String, String> payload) {
		this.payload = payload;
	}

	public boolean isCertificar() {
		return certificar;
	}

	public void setCertificar(boolean certificar) {
		this.certificar = certificar;
	}
	
	@Override
	public String toString(){
		return new Gson().toJson(this);
	}
	
	public String toStringPayload(){
		return new Gson().toJson(this.getPayload());
	}
	
	
}
