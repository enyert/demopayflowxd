package demo.models;

public class Status {
	private String operative;
	private String process; 
	
	public Status(){
		
	}
	
	public Status(String operative, String process) {
		this.operative = operative;
		this.process = process;
	}

	public String getOperative() {
		return operative;
	}

	public void setOperative(String operative) {
		this.operative = operative;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}
}
