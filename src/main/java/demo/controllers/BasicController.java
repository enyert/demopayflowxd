package demo.controllers;

import java.sql.SQLException;


import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import demo.models.Package;
import persistence.PersistenceHelper;
import util.DemoUtil;

@RestController
public class BasicController {
	DemoUtil demoUtil = new DemoUtil();
//	@RequestMapping("/")
//	public String index(String username, String password) throws SQLException{
//		int idUsuario = -1;
//		
//		idUsuario = demoUtil.login(username, password);
//		if(idUsuario != -1){
//			//buscar tareas no terminadas;
//			return demoUtil.getTareasNoTerminadas(idUsuario);
//		}else{
//			return "Usuario no tiene tareas pendientes";
//		}
//	}
	
	
	@RequestMapping(value="/paquete", method = RequestMethod.GET)
	@ResponseBody
	public List<Package> getAllPackage() throws SQLException{
		
		List<Package> listaPaquetes = demoUtil.getTareas();
		return listaPaquetes;
		
	}
	
	@RequestMapping(value="/paquete/{paqueteId}", method = RequestMethod.GET)
	@ResponseBody
	public Package getPackage(@PathVariable int paqueteId) throws SQLException{
		
		Package paquete = demoUtil.getPackage(paqueteId);
		return paquete;
		
	}
	
	@RequestMapping(value="/paquete/{paqueteId}", method = RequestMethod.PUT)
	@ResponseBody
	public Package putPackage(@PathVariable int paqueteId, @RequestBody String paquete) throws SQLException{
		//paquete.setId(id);
		Package newPaquete = demoUtil.updateJob2(paquete);
		return newPaquete;
		
	}	
}
